# Focus-iOS Frontend

Welcome to Focus-iOS-Frontend!

This repo is forked from https://bitbucket.org/terence_chen/ios-frontend, for seperate development on the account management feature of Focus.

## Requirements

* Detailed requirements can be found [here](http://docs.phonegap.com/en/2.5.0/guide_getting-started_ios_index.md.html)

1. Intel-based Computer with Mac® OS X® Lion or greater (10.7.4+)

2. Xcode 4.5 and iOS 6 SDK

3. Xcode Command Line Tools

4. iOS Device (for on-device test)

## Installation 

* Detailed guidelines can be found [here](http://docs.phonegap.com/en/2.5.0/guide_getting-started_ios_index.md.html)

1. Download and install the latest version of [PhoneGap](http://phonegap.com/download). 

2. Find the cordova-X.Y.Z-src.zip file under the folder where you downloaded the code. Extract the Cordova tool.

3. Create a new PhoneGap iOS project:
    
	`$ cordova create focus com.example.focus "Focus-ios-frontend"`
    `$ cd hello`
    `$ cordova platform add ios`
    `$ cordova prepare              # or "cordova build"`
	
4. Replace the content under the folder focus/platfroms/ios/www/ with the files in this repo.

## Test 

* Detailed guidelines can be found [here](http://docs.phonegap.com/en/2.5.0/guide_getting-started_ios_index.md.html)

1. Click the focus.xcodeproj file.

2. Use the xcode to navigate on-device or simulator test.
