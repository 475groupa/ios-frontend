/**
 * iOS Frontend/js/login.js`
 *
 * Worked on by: Silvery Fu (Team 1, Group A)
 *   2014-06-18: Created login.js, added basic login user interface
 *   2014-06-23: Added connection with the endpoint, met with the cross-domain ajax problem 
 *   2014-06-24: Revised the PhoneGap whitelisting mechanism, ajax request for login can be sent 
 *   2014-06-26: Merged the original logout.js into this file
 *   2014-06-28: Added support for ajax DELETE request, logout request can be sent
 *   2014-07-03: Added solution to csrf-token problem, both login and logout succeed
 *   2014-07-04: Added local storage acts as cookies; Refactored the code 
 */

$(function() {
  var form$ = $('#mform')
  , logout_bt$ = $('#mbutton')
  , url_base = 'http://proteus.mtomwing.com:13337';
  
  /* Store the cookie infomation as key-value pair in the local storage */
  function storeCookie(cookies) {
  clearCookie();
  $.each(cookies, function(key, val) {
         console.log(key);
         console.log(val);
         setCookie(key, val);
         });
  }
  
  function getCookie(co_name) {
    return localStorage.getItem(co_name);
  }
  
  function setCookie(co_key, co_value) {
    return localStorage.setItem(co_key, co_value);
  }
  
  function clearCookie() {
    return localStorage.clear();
  }
  
  /* Send the ajax login request, submitting the credentials */
  form$.submit(function(event) {
    var form_data = form$.serialize();
    event.preventDefault();
    $.ajax({
      type: 'POST',
      url: url_base + '/api/accounts/session',
      dataType: 'text',
      data: form_data,
      
      /* Set the csrf-token in the request header */
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
      },
      
      /* Store the csrf-token from the response header */
      success: function(data, textStatus, request) {
        console.log('Succesful login.');
        storeCookie($.parseJSON(data));
      },
      
      /* Clear the cookie if the previous cookie is expired */
      statusCode: {
        403: function() {
        clearCookie();
        }
      }
    });
  });
  
  /* Send the ajax logout request, clear the local storage */
  logout_bt$.click(function() {
    $.ajax({
      type: 'DELETE',
      url: url_base + '/api/accounts/session',
      dataType: 'text',
      
      beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
      },
      
      success: function(data, textStatus, request) {
        console.log('Succesful logout.');
        clearCookie();
      }           
    });
  });
});